#!/bin/bash 
echo ==== Starting Setup.sh Script ====
echo $HOME
echo ----------------------------------

yum update -y

curl -k -o ~/tsa_gallery.zip http://d3eglt6sb590rd.cloudfront.net/assets/tsa_gallery.zip
curl -k -o ~/setup.sql http://d3eglt6sb590rd.cloudfront.net/assets/setup.sql
curl -k -o ~/nginx.conf http://d3eglt6sb590rd.cloudfront.net/assets/nginx.conf

yum install git -y

yum install mariadb-server -y
systemctl enable mariadb
systemctl start mariadb
mysql -u root < ~/setup.sql

touch ~/.bashrc
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
source ~/.bashrc
nvm install 11.10.1
npm install -g forever

unzip ~/tsa_gallery.zip -d /opt/tsa_gallery
cd /opt/tsa_gallery
npm install
chmod -R 555 /opt/tsa_gallery
chmod -R 666 /opt/tsa_gallery/uploads
chmod -R 666 /opt/tsa_gallery/public/images/uploads


amazon-linux-extras install nginx1.12 -y
rm -f /etc/nginx/nginx.conf
mv ~/nginx.conf /etc/nginx/nginx.conf

systemctl start nginx
systemctl enable nginx

cd /opt/tsa_gallery
forever start ./bin/www.js

echo ==== Finished Setup.sh Script ====