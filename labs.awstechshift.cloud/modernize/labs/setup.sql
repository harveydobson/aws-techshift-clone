UPDATE mysql.user SET Password=PASSWORD('root') WHERE User='root';
DELETE FROM mysql.user WHERE User='';
DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
DROP DATABASE IF EXISTS test;
DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';

GRANT ALL PRIVILEGES ON *.* TO 'tsauser'@'localhost' IDENTIFIED BY 'my-secret-pw';

FLUSH PRIVILEGES;


CREATE DATABASE `tsagallery`;
use `tsagallery`;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(100) NOT NULL,
  `passwordsalt` varchar(45) NOT NULL,
  `displayname` varchar(100) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `createstamp` datetime NOT NULL,
  `updatestamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;


--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','4113894ade27a173a6bafd39611c3f3856faf07a7e4f8f85970fe5dcfb668750','a2btbsdpaf67cx7xe9ic6m7r1kqsnacp','Admin',1,now(),now());
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

-- Password is 2happymonkeys!

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `createstamp` datetime NOT NULL,
  `updatestamp` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_creator_idx` (`createdby`),
  CONSTRAINT `category_creator` FOREIGN KEY (`createdby`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Fiji',now(),now(),1);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
CREATE TABLE `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `tags` mediumtext NOT NULL,
  `createstamp` datetime NOT NULL,
  `updatestamp` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `categoryid` int(11) NOT NULL,
  `size` varchar(45) NOT NULL,
  `extension` varchar(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `image_category_idx` (`categoryid`),
  KEY `image_creator_idx` (`createdby`),
  CONSTRAINT `image_category` FOREIGN KEY (`categoryid`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `image_creator` FOREIGN KEY (`createdby`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` VALUES (1,'Sunset',2048,1367,'Transportation,Boat,Vehicle,Water,Port,Dock',now(),now(),1,1,'3806446','png'),(2,'Palm Trees',2048,1536,'Summer,Tropical,Arecaceae,Plant,Palm Tree,Tree',now(),now(),1,1,'4805031','png'),(3,'Beach',2048,1536,'Land,Outdoors,Nature,Summer,Ocean,Water',now(),now(),1,1,'4734863','png');
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

